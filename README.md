**UPDATE COMING SOON! NEW CHALLENGES, MORE LINKS!**

`Last Updated: September 8 2019`  
*I will update the date above only when the content has changed.*

## CTF-101

This page contains the tools used in the presentation. Refer to "The Book of Secret Knowledge" for an extensive list of tools and information!

The slides will be hosted here shortly.

## 0. Resources
`The Book of Secret Knowledge`  -   https://github.com/trimstray/the-book-of-secret-knowledge  
`Captf's CTF list`              -   http://captf.com/practice-ctf/  

## 1. Reverse-Engineering  

`Ghidra`        -       https://ghidra-sre.org/ & https://github.com/NationalSecurityAgency/ghidra  
`radare2`       -       https://github.com/radare/radare2  
`Cutter`        -       https://github.com/radareorg/cutter  
`gdb`           -       https://github.com/longld/peda  
`Flare VM`      -       https://www.fireeye.com/services/freeware/flare-vm.html & https://github.com/fireeye/flare-vm  

## 2. Cryptography  

`CyberChef`     -       https://gchq.github.io/CyberChef/  

## 3. Python Programming  

`ipython`       -       https://pypi.org/project/ipython/  

## 4. Password Cracking  

`fcrackzip`     -       https://github.com/hyc/fcrackzip or "apt install fcrackzip"  

## 5. Powershell
`Chocolatey`    -       https://chocolatey.org/